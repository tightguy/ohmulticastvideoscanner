Oh! Multicast Video Scanner

You can find free video streaming multicast ip addresses with Oh! Multicast
Video Scanner.

omvs scans all multicast ip addresses the user specifies and if omvs finds
something, omvs tries to decode it with gstreamer and saves it as png file in
the output directory. There are many cases omvs finds something but png file is
not stored. Such as, the video is scrambled(encrypted, not free), multicasting
data doesn't contain video(audio only, data only), gstreamer doesn't recognize
it(unknown codec), etc. If png file is successfully stored, the multicast ip
address is stored in an m3u file in the output directory.

Disable all network device interfaces except for one network device interface
is recommended, which means disabling all wifi network device interfaces and
disabling all unused ethernet network device interfaces.

With "-i" option, you can decide which network device interface is used. Use the
dev_idx from "-l" option output.
With "-l" option, you can see the available network device interface list.
With "-j" option, you can change the number of jobs. If you increase the number
of jobs, you can scan faster. However, You need take caution, because it can
cause network congestion.
With "-o" option, you can change the output directory. The default directory is
"omvs_out".
With "-s" option, you can change how long you sleep after finishing scanning an
multicast ip address before starting scanning the next ip address. The default
is 1000ms(1 second).
With "-t" option, you can change the scan timeout. The default scan timeout is
10000ms(10 seconds). It tries to get png image file while scanning. If it can't
get the png image file for the timeout, it gives up and tries the next
multicast ip address.
With "-u" option, you can scan with udp protocol instead of rtp protocol. The
default is scanning with rtp protocol.
With "-q" option, you can disable the log printing.
With "-w" option, you can wait for the multicast packet without specifying the
scanning ip addresses. When it detects multicast packet from the specified
network device, it tries to scan the ip address from multicast packet and save
the png file from the multicast ip address. In order to use "-w" option,
the network device interface need to be connected where it can sniff multicast
packet. The environment such as connection to switch hub without igmp-snooping
function is necessary. Press Ctrl-C if you want to stop waiting.

As omvs uses libpcap(or WinPcap on MS Windows) to capture raw packet, root
privilege is required to run omvs.

Run "./configure ; make" to build omvs. Run "./omvs" with scanning multicast ip
addresses. For the detailed build information, consult INSTALL, INSTALL.ubuntu,
or INSTALL.windows.

e.g.)
Scanning multicast ip addresses.
-------------------------------------------------------------------------------
$ ifconfig
eth0      Link encap:Ethernet  HWaddr 80:ee:73:a2:58:4b  
          inet addr:172.30.1.201  Bcast:172.30.1.255  Mask:255.255.255.0
          inet6 addr: fe80::82ee:73ff:fea2:584b/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:41357508 errors:0 dropped:5 overruns:0 frame:0
          TX packets:827665 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:56530625218 (56.5 GB)  TX bytes:99250131 (99.2 MB)

eth1      Link encap:Ethernet  HWaddr 80:ee:73:a2:58:4a  
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:38983 errors:0 dropped:0 overruns:0 frame:0
          TX packets:38983 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:15278201 (15.2 MB)  TX bytes:15278201 (15.2 MB)

$ sudo ./omvs -l
dev_idx(0) dev_name(eth0) dev_desc((null))
dev_idx(1) dev_name(bluetooth0) dev_desc(Bluetooth adapter number 0)
dev_idx(2) dev_name(nflog) dev_desc(Linux netfilter log (NFLOG) interface)
dev_idx(3) dev_name(nfqueue) dev_desc(Linux netfilter queue (NFQUEUE) interface)
dev_idx(4) dev_name(eth1) dev_desc((null))
dev_idx(5) dev_name(any) dev_desc(Pseudo-device that captures on all interfaces)
dev_idx(6) dev_name(lo) dev_desc((null))
$ sudo ./omvs -i 0 233.19.187.192/30 233.19.187.244 233.19.187.1
openning omvs_out/omvs_67883420797.m3u
[0x18c6230] start scanning 233.19.187.192
[0x18c6230] trying to save rtp://233.19.187.192:5000 to omvs_out/233.19.187.192-5000.png
[0x18c6230] finish scanning 233.19.187.192
[0x18c6230] start scanning 233.19.187.193
[0x18c6230] trying to save rtp://233.19.187.193:5000 to omvs_out/233.19.187.193-5000.png
[0x18c6230] finish scanning 233.19.187.193
[0x18c6230] start scanning 233.19.187.194
[0x18c6230] trying to save rtp://233.19.187.194:5000 to omvs_out/233.19.187.194-5000.png
[0x18c6230] finish scanning 233.19.187.194
[0x18c6230] start scanning 233.19.187.195
[0x18c6230] trying to save rtp://233.19.187.195:5000 to omvs_out/233.19.187.195-5000.png
[0x18c6230] finish scanning 233.19.187.195
[0x18c6230] start scanning 233.19.187.244
[0x18c6230] trying to save rtp://233.19.187.244:5000 to omvs_out/233.19.187.244-5000.png
[0x18c6230] finish scanning 233.19.187.244
[0x18c6230] start scanning 233.19.187.1
[0x18c6230] finish scanning 233.19.187.1
$ ls -l omvs_out
total 1692
-rw-r--r-- 1 root root 442585 Mar  3 01:54 233.19.187.193-5000.png
-rw-r--r-- 1 root root 600331 Mar  3 01:54 233.19.187.194-5000.png
-rw-r--r-- 1 root root 217721 Mar  3 01:54 233.19.187.195-5000.png
-rw-r--r-- 1 root root 458283 Mar  3 01:54 233.19.187.244-5000.png
-rw-r--r-- 1 root root    160 Mar  3 01:54 omvs_67883420797.m3u
$ cat omvs_out/omvs_67883420797.m3u
#EXTM3U
#EXTINF:1,1
rtp://233.19.187.193:5000
#EXTINF:2,2
rtp://233.19.187.194:5000
#EXTINF:3,3
rtp://233.19.187.195:5000
#EXTINF:4,4
rtp://233.19.187.244:5000
$ 
-------------------------------------------------------------------------------
Waiting for the multicast packet.
-------------------------------------------------------------------------------
$ ifconfig
eth0      Link encap:Ethernet  HWaddr 80:ee:73:a2:58:4b  
          inet6 addr: fe80::82ee:73ff:fea2:584b/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:41357843 errors:0 dropped:5 overruns:0 frame:0
          TX packets:828004 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:56530695954 (56.5 GB)  TX bytes:99292621 (99.2 MB)

eth1      Link encap:Ethernet  HWaddr 80:ee:73:a2:58:4a  
          inet addr:183.101.193.195  Bcast:183.101.193.255  Mask:255.255.255.0
          inet6 addr: fe80::82ee:73ff:fea2:584a/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:408 errors:0 dropped:0 overruns:0 frame:0
          TX packets:149 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:44532 (44.5 KB)  TX bytes:23688 (23.6 KB)

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:39301 errors:0 dropped:0 overruns:0 frame:0
          TX packets:39301 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:15311882 (15.3 MB)  TX bytes:15311882 (15.3 MB)

$ sudo ./omvs -l
dev_idx(0) dev_name(eth0) dev_desc((null))
dev_idx(1) dev_name(bluetooth0) dev_desc(Bluetooth adapter number 0)
dev_idx(2) dev_name(nflog) dev_desc(Linux netfilter log (NFLOG) interface)
dev_idx(3) dev_name(nfqueue) dev_desc(Linux netfilter queue (NFQUEUE) interface)
dev_idx(4) dev_name(eth1) dev_desc((null))
dev_idx(5) dev_name(any) dev_desc(Pseudo-device that captures on all interfaces)
dev_idx(6) dev_name(lo) dev_desc((null))
$ sudo ./omvs -i 4 -w
openning omvs_out/omvs_68456824735.m3u
[0x1029840] start scanning 233.18.158.252
[0x1029840] trying to save rtp://233.18.158.252:1901 to omvs_out/233.18.158.252-1901.png
[0x1029840] finish scanning 233.18.158.252
[0x1029840] start scanning 233.14.173.241
[0x1029840] trying to save rtp://233.14.173.241:5000 to omvs_out/233.14.173.241-5000.png
[0x1029840] finish scanning 233.14.173.241
[0x1029840] start scanning 233.18.158.250
[0x1029840] trying to save rtp://233.18.158.250:1902 to omvs_out/233.18.158.250-1902.png
[0x1029840] finish scanning 233.18.158.250
[0x1029840] start scanning 233.18.158.206
[0x1029840] trying to save rtp://233.18.158.206:5000 to omvs_out/233.18.158.206-5000.png
[0x1029840] finish scanning 233.18.158.206
[0x1029840] start scanning 233.18.158.75
[0x1029840] trying to save rtp://233.18.158.75:5000 to omvs_out/233.18.158.75-5000.png
[0x1029840] finish scanning 233.18.158.75
^C$ ls -l omvs_out
total 3876
-rw-r--r-- 1 root root 1917823 Mar  3 02:05 233.18.158.206-5000.png
-rw-r--r-- 1 root root 2040917 Mar  3 02:07 233.18.158.75-5000.png
-rw-r--r-- 1 root root      83 Mar  3 02:07 omvs_68456824735.m3u
$ cat omvs_out/omvs_68456824735.m3u
#EXTM3U
#EXTINF:1,1
rtp://233.18.158.206:5000
#EXTINF:2,2
rtp://233.18.158.75:5000
$
-------------------------------------------------------------------------------

For more detail, please run omvs --help.

This program is distributed under GPLv3. If you can't conform to GPLv3,
please contact me to discuss about other license options.

Written by Taeho Oh <ohhara@postech.edu>
http://ohhara.sarang.net/omvs
